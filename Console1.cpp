﻿#include<stdio.h>
#include<Windows.h>
#include<math.h>
#include<time.h>
#include<random>


int main() 
{

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	unsigned int arr[50000], arr2[300] = { 0 }, a = 48271, c = 0;
	unsigned long long	m = pow(2, 32);
	double arr3[300] = { 0 }, arr4[300] = { 0 }, M = 0, D = 0;

	arr[0] = time(0);
	for (int i = 1; i < 50000; i++)
	{
		arr[i] = (a * arr[i - 1]) % m;
		arr[i - 1] = arr[i - 1] % 300;
		printf("%d\t", arr[i - 1]);
		arr2[arr[i - 1]]++;
	}
	arr[49999] = arr[49999] % 300;
	arr2[arr[49999]]++;


	for (int i = 0; i < 300; i++)
	{
		if (arr2[i] != 0)
		{
			printf("\n%d -> %d", i, arr2[i]);
		}
	}

	for (int i = 0; i < 300; i++)
	{
		if (arr2[i] != 0)
		{
			arr3[i] = arr2[i] / 50000.0;
			M += arr3[i] * i;
			printf("\nСтатистична ймовірність %d -> %f", i, arr3[i]);
		}
	}

	for (int i = 0; i < 300; i++)
	{
		if (arr2[i] != 0)
		{
			D += pow((i - M), 2) * arr3[i];
		}
	}
	printf("\nМатематичне сподівання = %f", M);
	printf("\nДисперсія = %f", D);
	printf("\nСередньоквадратичне відхилення = %f", pow(D, 1 / 2.));


	return 0;
}
